import { Object3D, PlaneGeometry, MeshBasicMaterial, Mesh, VertexColors, Color } from 'three';
import chroma = require('chroma-js');
import noisejs = require('noisejs');

class Terrain extends Object3D {
    private geometry:PlaneGeometry;
    private material:MeshBasicMaterial;
    private mesh:Mesh;

    private MAX_HEIGHT:number = 2;
    private EASE_FACTOR:number = .015;

    constructor (width?: number, height?: number, widthSegments?: number, heightSegments?: number)
    {
        super();
        this.geometry = new PlaneGeometry(width, height, widthSegments, heightSegments);

        // Apply height
        let noise = new noisejs.Noise(Math.random());
        let planeWidth = widthSegments + 1;
        for(let i = 0; i < this.geometry.vertices.length;i++) {
            this.geometry.vertices[i].z = noise.simplex2((i % planeWidth) * this.EASE_FACTOR, Math.floor(i / planeWidth) * this.EASE_FACTOR) * this.MAX_HEIGHT;
        }

        // Apply color
        let scale = chroma.scale(['darkgreen', 'green', 'white']).domain([0, this.MAX_HEIGHT]);
        for (let i = 0; i < this.geometry.faces.length; ++i) {
            this.geometry.faces[i].color = new Color(scale(Math.max(this.geometry.vertices[this.geometry.faces[i].a].z,
                                                                    this.geometry.vertices[this.geometry.faces[i].b].z,
                                                                    this.geometry.vertices[this.geometry.faces[i].c].z)).hex());
        }

        this.material = new MeshBasicMaterial({
            vertexColors: VertexColors
        });

        this.mesh = new Mesh(this.geometry, this.material);
        this.add(this.mesh);
    }

    render () {
    }
}

export default Terrain;