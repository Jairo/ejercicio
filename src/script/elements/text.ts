import { Object3D, Mesh, MeshBasicMaterial, Texture, Color, PlaneGeometry, DoubleSide, Box3, Vector3 } from 'three';

class Text extends Object3D {
    private mesh:Mesh;
    material:MeshBasicMaterial;
    private texture:Texture;

    constructor (text:string, fontSize:number = 14, fontFamily:string = 'Arial', color:Color = new Color('white'))
    {
        super();

        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        context.font = fontSize + 'px ' + fontFamily;
        context.fillStyle = 'rgba(' + color.r * 255 + ',' + color.g * 255 + ',' + color.b * 255 + ',1)';
        context.fillText(text, 0, fontSize);

        this.texture = new Texture(canvas);
        this.texture.needsUpdate = true;

        this.material = new MeshBasicMaterial( {map: this.texture, side:DoubleSide} );
        this.material.opacity = 0;
        this.material.transparent = true;

        this.mesh = new Mesh(new PlaneGeometry(canvas.width, canvas.height), this.material);
        this.mesh.position.set(120,-70, 0);   // TODO find a way to avoid using magic numbers
        this.add(this.mesh);
    }

    render () {
    }
}

export default Text;