var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var watchify = require("watchify");
var tsify = require('tsify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var gutil = require("gulp-util");

var watchedBrowserify  = watchify(browserify({
    basedir: '.',
        debug: true,
        entries: ['src/script/main.ts'],
        cache: {},
        packageCache: {}
    })
.plugin(tsify));

function watch() {
    return watchedBrowserify
		.transform('babelify', {
			presets: ['es2015'],
			extensions: ['.ts']
		})
        .bundle()
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(gulp.dest('build'));
}

watchedBrowserify.on("update", watch);
watchedBrowserify.on("log", gutil.log);
gulp.task('watch', watch);